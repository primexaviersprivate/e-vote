<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nim' => 000000000000,
            'name' => "Administrator",
            'username' => "Administrator",
            'card_no' => 000000000000,
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
        ]);
        DB::table('user_data')->insert([
            'name' => "User 1",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 2",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 3",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 4",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 5",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 6",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 7",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('user_data')->insert([
            'name' => "User 8",
            'generation' => 2016,
            'major' => "Mekatronika",
        ]);
        DB::table('organizations')->insert([
            'name' => "Organisasi 1",
            'description' => "Organisasi 1",
        ]);
        DB::table('organizations')->insert([
            'name' => "Organisasi 2",
            'description' => "Organisasi 2",
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 1,
            'organization_id' => 1,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 2,
            'organization_id' => 1,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 3,
            'organization_id' => 1,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 4,
            'organization_id' => 1,
            'is_active' => true,
        ]);        
        DB::table('candidate_data')->insert([
            'user_data_id' => 5,
            'organization_id' => 2,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 6,
            'organization_id' => 2,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 7,
            'organization_id' => 2,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 8,
            'organization_id' => 2,
            'is_active' => true,
        ]);
        DB::table('candidate_data')->insert([
            'user_data_id' => 8,
            'organization_id' => 2,
            'is_active' => true,
        ]);        
        DB::table('election_periods')->insert([
            'election_type' => 'legislatif',
            'status' => true,
            'start_date' => Carbon\Carbon::now(),
            'end_date' => Carbon\Carbon::now()->addYear(10),
        ]);           
        DB::table('election_periods')->insert([
            'election_type' => 'eksekutif',
            'status' => true,
            'start_date' => Carbon\Carbon::now(),
            'end_date' => Carbon\Carbon::now()->addYear(10),
        ]);      
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 1,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);      
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 2,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 3,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 4,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 5,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 6,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 7,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 8,
            'election_periods_id' => 1,
            'candidate_type' => 'legislatif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 1,
            'election_periods_id' => 1,
            'candidate_type' => 'eksekutif',
        ]);
        DB::table('candidate_periods')->insert([
            'candidate_data_id' => 2,
            'election_periods_id' => 1,
            'candidate_type' => 'eksekutif',
        ]);
    }
}
