<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("action");
            $table->bigInteger("user_id")->nullable();
            $table->string("value")->nullable();
            $table->boolean("is_success");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_actions');
    }
}
