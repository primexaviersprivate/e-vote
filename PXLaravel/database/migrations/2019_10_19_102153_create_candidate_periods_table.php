<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('candidate_data_id');
            $table->biginteger('election_periods_id');
            $table->enum('candidate_type',['legislatif','eksekutif']);
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_periods');
    }
}
