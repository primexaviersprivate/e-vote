<?php

use App\Model\DeviceAction;
use App\Model\ElectionPeriod;
use App\Model\CandidatePeriod;
use App\Model\VotingCandidate;
use App\Model\Organization;
use Carbon\Carbon;

Route::get('/welcome', function () {
    return view('index');
})->name("index");

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('e-vote')->group(function () {
    Route::get('/', function () {
        return view('project');
    })->name("project");    
    Route::get('/tap-kartu', function () {
        // echo File::get(storage_path('app/scannerFile.txt')); 
        // $time = Storage::lastModified('scannerFile.txt');

        // echo $time;
        // dd();
        if(Auth::user()){
            return redirect()->route('tipe-pemilih');
        }
        $deviceActions = DeviceAction::where("action",2)->Where("is_success",false)->get();
        foreach($deviceActions as $deviceAction){
            $deviceAction->action = 0;
            $deviceAction->save();
        }
        $newdeviceAction = new DeviceAction;
        $newdeviceAction->action = 2;
        $newdeviceAction->value = "";
        $newdeviceAction->is_success = false;
        $newdeviceAction->save();
        return view('1-LoginPemilih');
    })->name("tap-kartu");    
    Route::get('/tipe-pemilih', function () {
        $elections = ElectionPeriod::WhereDate('start_date','>=',Carbon::today())->whereDate("end_date",">=",Carbon::today())->get();
        return view('2-tipe-pemilu')->with("data",$elections);
    })->name("tipe-pemilih"); 
    Route::get('/pilih-eksekutif', function () {
        if(!Auth::user()){
            return redirect()->route('tap-kartu');
        }
        $checkVote = VotingCandidate::where('user_id',Auth::User()->id)->get();
        $isAlreadyVote = false;
        foreach($checkVote as $vote){
            $electiontype = CandidatePeriod::where('id',$vote->candidate_period_id)->first();
            if($electiontype){
                if($electiontype->candidate_type == 'eksekutif'){
                    $isAlreadyVote = true;
                }
            }
        }
        if($isAlreadyVote){
            return redirect()->route('tipe-pemilih');       
        }else{
            $candidate = CandidatePeriod::where("candidate_type","eksekutif")->get();
            return view('3_1-pilih-eksekutif')->with('data',$candidate);
        }
    })->name("pilih-eksekutif");   
    Route::get('/pilih-legislatif', function () {
        if(!Auth::user()){
            return redirect()->route('tap-kartu');
        }
        $checkVote = VotingCandidate::where('user_id',Auth::User()->id)->get();
        $isAlreadyVote = false;
        foreach($checkVote as $vote){
            $electiontype = CandidatePeriod::where('id',$vote->candidate_period_id)->first();
            if($electiontype){
                if($electiontype->candidate_type == 'legislatif'){
                    $isAlreadyVote = true;
                }
            }
        }
        if($isAlreadyVote){
            return redirect()->route('tipe-pemilih');           
        }else{
            $candidate = CandidatePeriod::where("candidate_type","legislatif")->with('CandidateData')->get();
            $organization = Organization::get();
            return view('3_2-pilih-legislatif')->with('data',$candidate)->with('organizations',$organization);
        }})->name("pilih-legislatif");  
    Route::get('/voting-legislatif/{id}',function ($id) {
        if(!Auth::user()){
            return redirect()->route('tap-kartu');
        }
        $candidate = CandidatePeriod::where("id",$id)->first();
        if($candidate){
            if($candidate->candidate_type == "legislatif"){
                $newVoting = new VotingCandidate;
                $newVoting->user_id = Auth::user()->id;
                $newVoting->candidate_period_id = $id;
                $newVoting->voting_time = Carbon::now();    
                if($newVoting->save()){
                    return redirect()->route('hasil-voting');
                }else{
                    return redirect()->route('tap-kartu');
                }
            }else{
                return redirect()->route('tap-kartu');
            }
        }else{
            return redirect()->route('tap-kartu');
        }
    })->name("voting-legislatif"); 
    Route::get('/voting-eksekutif/{id}', function ($id) {
        if(!Auth::user()){
            return redirect()->route('tap-kartu');
        }
        $candidate = CandidatePeriod::where("id",$id)->first();
        if($candidate){
            if($candidate->candidate_type == "eksekutif"){
                $newVoting = new VotingCandidate;
                $newVoting->user_id = Auth::user()->id;
                $newVoting->candidate_period_id = $id;
                $newVoting->voting_time = Carbon::now();
                if($newVoting->save()){
                    return redirect()->route('hasil-voting');
                }else{
                    return redirect()->route('tap-kartu');
                }
            }else{
                return redirect()->route('tap-kartu');
            }
        }else{
            return redirect()->route('tap-kartu');
        }
    })->name("voting-eksekutif");  
    Route::prefix('list')->group(function () {
        Route::get('/user', function () {
            return view('listUser');
        })->name("list-user");  
        Route::get('/kandidat', function () {
            return view('listKandidat');
        })->name("list-kandidat");  
        Route::get('/organisasi', function () {
            return view('listOrganisasi');
        })->name("list-organisasi");  
        Route::get('/voting', function () {
            return view('listVoting');
        })->name("list-voting");  
    });
    Route::prefix('hasil')->group(function () {
        Route::get('/Voting', function () {
            $hasil = VotingCandidate::get();
            $candidatePeriod = CandidatePeriod::get();
            return view('hasilVoting')->with('candidates',$candidatePeriod)->with("hasilVoting",$hasil);
        })->name("hasil-voting");  
    });
});

Route::get('/datatables', 'DatatablesController@anyData')->name("datatables.data"); 
Route::get('/datatables/kandidat', 'DatatablesController@listKandidat')->name("datatables.kandidat"); 
Route::get('/datatables/organisasi', 'DatatablesController@listOrganisasi')->name("datatables.organisasi");  
Route::get('/datatables/voting', 'DatatablesController@listVoting')->name("datatables.voting");  
Route::get('/datatables/user', 'DatatablesController@listUser')->name("datatables.user");  
