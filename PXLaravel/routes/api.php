<?php

use App\Model\User;
use App\Model\DeviceAction;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user/add', function (request $request){
    return User::all();
});

Route::post('/user/scan', function (request $request){
    if($request->nfcData){
        $selectDeviceAction = DeviceAction::where("is_success",false)->where("action",2)->first();
        if($selectDeviceAction){
            $selectDeviceAction->is_success = true;
            $selectDeviceAction->value = $request->nfcData;
            if($selectDeviceAction->save()){
                Storage::put('scannerFile.txt', $request->nfcData);
                return "1";
            }else{
                return "2";
            }
        }else{
            return "0";
        }
    }
    else{
        return "0"; 
    }
});

Route::get("/device/action", function (request $request){
    $deviceStatus = DeviceAction::where("is_success",false)->where("action","<>",0)->orderBy("id","asc")->first();
    if($deviceStatus){
        if($deviceStatus->action == 1){
            return "1";
        }else if($deviceStatus->action == 2){
            return "2";
        }else{
            return "0";
        }
    }else{
        return "0";
    }
});

Route::post('/user/add/cardID', function (request $request){
    if(!empty($request->CardData)){
        if(!empty($request->nim)){
            $user = User::where("nim",$request->nim)->first();
            if($user){
                $user->card_no = $request->CardData;
                $user->save();
                return "success";
            }else{
                return "User Not Found Not Added";
            }
        }else{
            $deviceAction = DeviceAction::where("action",1)->where("status",false)->first();
            if($deviceAction){
                $user = User::where("id",$deviceAction->user_id)->first();
                if($user){
                    $user->card_no = $request->CardData;
                    $user->save();
                    $deviceAction->value = $request->CardData;
                    $deviceAction->status = true;
                    $deviceAction->save();
                    return "success";
                }else{
                    return "failed";
                }
            }else{
                $user = new User;
                $user->name = Str::random(10);
                $user->username = $request->CardData;
                $user->card_no = $request->CardData;
                $user->email = Str::random(10).'@gmail.com';
                $user->password = bcrypt($request->CardData);
                $user->save();    
                return "success";
            }
            return "Nim is Empty Create New User";
        }
    }
    return "Request Failed";
});

