@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-md-center">Untuk memilih</div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="cardID" class="col-md-12 col-form-label text-md-center">Tap Kartu Anda!!!</label>
                        <label for="cardID" class="col-md-3 col-form-label text-md-center"></label>
                        <img class="col-md-6" src="../Frontend/img/kartu.png" alt="">
                        <label for="cardID" class="col-md-3 col-form-label text-md-center"></label>
                        <label for="cardID" class="col-md-12 col-form-label text-md-center">Atau</label>
                        <label for="cardID" class="col-md-12 col-form-label text-md-center">Masukan Username dan Password anda</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
