@extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">silahkan pilih legislatif anda</div>
                <div class="card-body">
                    <div class="form-group row">
                        @foreach($organizations as $organization)
                        <div class="col-md-4">
                            <table border="1" width="90%">
                                <thead align="center">
                                    <tr>
                                        <th>{{$organization->name}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;  ?>
                                    @foreach($data as $candidate)
                                        @if($candidate->CandidateData->organization_id == $organization->id)
                                            <tr onclick="window.location.href='{{route('voting-legislatif',$candidate->id)}}'">
                                                <td>{{$i}}. <?php echo App\Model\CandidateData::where('id',$candidate->CandidateData->user_data_id)->with('UserData')->first()->UserData->name; ?></td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection