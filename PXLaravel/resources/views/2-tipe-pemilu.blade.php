@extends('layouts.app')

@section('content')
@foreach($data as $isidata)
    @if($isidata->election_type == "legislatif")
        <?php $islegislatif = true ?>
    @endif
    @if($isidata->election_type == "eksekutif")
        <?php $iseksekutif = true ?>
    @endif
@endforeach
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md12">
            <div class="card">
                <div class="card-header">Untuk memilih tolong tap kartu anda.</div>
                <div class="card-body">
                    <div class="form-group row">
                        @if($islegislatif)
                        <div class="col-md-6 justify-content-center"> 
                            <div class="col-md-9" style="cursor:pointer" onclick="window.location='{{ route('pilih-eksekutif') }}';">
                                <img class="col-md-12" src="../Frontend/img/president.png" alt="">
                                <label for="cardID" class="col-md-12 col-form-label text-md-center  ">Pemilu Eksekutif</label>                            
                            </div>
                        </div>
                        @endif
                        @if($iseksekutif)
                        <div class="col-md-6 justify-content-center">
                            <div class="col-md-9" style="cursor:pointer" onclick="window.location='{{ route('pilih-legislatif') }}';">
                                <img class="col-md-12" src="../Frontend/img/president.png" alt="">
                                <label for="cardID" class="col-md-12 col-form-label text-md-center">Pemilu Legislatif</label>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
