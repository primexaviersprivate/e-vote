<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <title>Jejak Rizky</title>
    <!--
Raleway Template 
http://www.templatemo.com/preview/templatemo_421_raleway
-->
    <link rel="stylesheet" href="Frontend/css/bootstrap.min.css">
    <link rel="stylesheet" href="Frontend/css/font-awesome.min.css">
    <link rel="stylesheet" href="Frontend/css/simple-line-icons.css">
    <link rel="stylesheet" href="Frontend/css/animate.css">
    <link rel="stylesheet" href="Frontend/css/templatemo_style.css">
</head>

<body>
    <header class="site-header container animated fadeInDown">
        <div class="header-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="site-branding">
                        <a href="#"><h1>JejakRizky</h1></a>
                    </div>
                </div>
                <a href="#" class="toggle-nav hidden-md hidden-lg">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="col-md-8">
                    <nav id="nav" class="main-navigation hidden-xs hidden-sm">
                        <ul class="main-menu">
                            <li><a class="show-1 homebutton" href="{{ route('index') }}">Home</a></li>
                            <li><a class="show-2 aboutbutton" href="#">About Us</a></li>
                            <li><a class="show-3 active projectbutton" href="{{ route('project') }}">Projects</a></li>
                            <li><a class="show-4 blogbutton" href="#">Blog Entries</a></li>
                            <li><a class="show-5 contactbutton" href="#">Contact</a></li>
                        </ul>
                    </nav>
                    <nav class="main-navigation menu-responsive hidden-md hidden-lg">
                        <ul class="main-menu">
                            <li><a class="show-1 homebutton" href="{{ route('index') }}">Home</a></li>
                            <li><a class="show-2 aboutbutton" href="#">About Us</a></li>
                            <li><a class="show-3 active projectbutton" href="{{ route('project') }}">Projects</a></li>
                            <li><a class="show-4 blogbutton" href="#">Blog Entries</a></li>
                            <li><a class="show-5 contactbutton" href="#">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div id="menu-container">
        <div id="menu-3" class="homepage home-section container">
            <div class="home-projects">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="project-title animated fadeInUp">
                            <h2>Latest Projects</h2>
                            <p>Sed eu turpis vehicula, iaculis sapien eu, molestie libero. Cras ac urna in neque commodo sodales vel et dolor. Pellentesque aliquam semper lectus, nec consequat ex lacinia nec.
                                <br>
                                <br>Nunc eget velit nec felis ultrices vulputate venenatis interdum arcu. In ac auctor quam. </p>
                            <a href="#" class="pink-button">Continue Journal</a>
                        </div>
                    </div>
                    <div class="project-home-holder col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="project-item one animated fadeInRight">
                                    <img src="Frontend/img/1.jpg" alt="">
                                    <div class="overlay">
                                        <h4><a href="#">Project One</a></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="project-item two animated fadeInRight">
                                    <img src="Frontend/img/2.jpg" alt="">
                                    <div class="overlay">
                                        <h4><a href="#">Project Two</a></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="project-item three animated fadeInRight">
                                    <img src="Frontend/img/3.jpg" alt="">
                                    <div class="overlay">
                                        <h4><a href="#">Project Three</a></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="project-item four animated fadeInRight">
                                    <img src="Frontend/img/4.jpg" alt="">
                                    <div class="overlay">
                                        <h4><a href="#">Project Four</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="site-footer container text-center">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-footer">
                        <ul class="social">
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 copyright">
                    <p>Copyright &copy; 2084 <a href="#">Company Name</a></p>
                </div>
            </div>
        </footer>
        <!-- templatemo 421 raleway -->
        <span class="border-top"></span>
        <span class="border-left"></span>
        <span class="border-right"></span>
        <span class="border-bottom"></span>
        <span class="shape-1"></span>
        <span class="shape-2"></span>

        <script src="Frontend/js/jquery.min.js"></script>
        <script src="Frontend/js/templatemo_custom.js"></script>
</body>

</html>