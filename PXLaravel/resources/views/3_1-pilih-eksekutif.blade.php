@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">silahkan pilih calon presiden anda</div>
                <div class="card-body">
                    <div class="form-group row">
                        @foreach($data as $dataCandidate)
                            <div class="col-md-6" onclick="window.location.href='{{route('voting-eksekutif',$dataCandidate->id)}}'">
                                <img class="col-md-12" src="../Frontend/img/1.png" alt="">
                                <label for="cardID" class="col-md-12 col-form-label text-md-center">capres & cawapres no {{$dataCandidate->id}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
