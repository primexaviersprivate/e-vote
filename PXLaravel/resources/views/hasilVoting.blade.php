@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Hasil Voting</div>
                Hasil Legislatif : </br>
                <?php
                $jumlahLegis = 0;
                $jumlahEkse = 0;
                ?>
                @foreach($candidates as $candidate)
                    @if($candidate->candidate_type == "legislatif") 
                        <?php echo App\Model\CandidateData::where('id',$candidate->candidate_data_id)->first()->UserData->name; ?> = <?php echo App\Model\VotingCandidate::where('candidate_period_id',$candidate->id)->get()->count(); $jumlahLegis += App\Model\VotingCandidate::where('candidate_period_id',$candidate->id)->get()->count(); ?></br>
                    @endif
                @endforeach
                <br>
                Total : {{$jumlahLegis}}</br></br>
                
                Hasil Eksekutif : </br>
                @foreach($candidates as $candidate)
                    @if($candidate->candidate_type == "eksekutif") 
                        <?php echo App\Model\CandidateData::where('id',$candidate->candidate_data_id)->first()->UserData->name; ?> = <?php echo App\Model\VotingCandidate::where('candidate_period_id',$candidate->id)->get()->count(); $jumlahEkse += App\Model\VotingCandidate::where('candidate_period_id',$candidate->id)->get()->count(); ?></br>
                    @endif
                @endforeach
                
                </br>
                Total : {{$jumlahEkse}}</br></br>
                

            </div>
        </div>
    </div>
</div>
@endsection