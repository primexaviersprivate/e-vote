<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\CandidateData;
use App\Model\Organization;
use App\Model\VotingCandidate;
use DataTables;

class DatatablesController extends Controller
{
    public function getIndex()
    {
        return view('datatables.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listUser()
    {
        return Datatables::of(User::get())->make(true);
    }

    public function listKandidat()
    {
        return Datatables::of(CandidateData::with('UserData')->get())
        ->editColumn('name', function ($orders) {
            return $orders->UserData->name;
        })->make(true);
    }

    public function listOrganisasi()
    {
        return Datatables::of(Organization::query())->make(true);
    }

    public function listVoting()
    {
        return Datatables::of(VotingCandidate::query())->make(true);
    }
}
