<?php

namespace App\Http\Controllers;

use App\Model\CandidateData;
use Illuminate\Http\Request;

class CandidateDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\CandidateData  $candidateData
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateData $candidateData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\CandidateData  $candidateData
     * @return \Illuminate\Http\Response
     */
    public function edit(CandidateData $candidateData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\CandidateData  $candidateData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateData $candidateData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\CandidateData  $candidateData
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateData $candidateData)
    {
        //
    }
}
