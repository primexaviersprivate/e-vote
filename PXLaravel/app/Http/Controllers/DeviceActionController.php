<?php

namespace App\Http\Controllers;

use App\Model\DeviceAction;
use Illuminate\Http\Request;

class DeviceActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DeviceAction  $deviceAction
     * @return \Illuminate\Http\Response
     */
    public function show(DeviceAction $deviceAction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DeviceAction  $deviceAction
     * @return \Illuminate\Http\Response
     */
    public function edit(DeviceAction $deviceAction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DeviceAction  $deviceAction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeviceAction $deviceAction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DeviceAction  $deviceAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeviceAction $deviceAction)
    {
        //
    }
}
