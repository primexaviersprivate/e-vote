<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidatePeriod extends Model
{
    public function CandidateData()
    {
        return $this->hasOne('App\Model\CandidateData','id','candidate_data_id');
    }
}
