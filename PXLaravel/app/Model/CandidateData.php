<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidateData extends Model
{
    public function UserData()
    {
        return $this->hasOne('App\Model\UserData','id','user_data_id');
    }
}
